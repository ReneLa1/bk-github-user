import styled from "styled-components";

const LogoImage = styled.img`
  height: 20vmin;
  pointer-events: none;
  border-radius: 100%;
  "&:hover": {
  }
`;

export { LogoImage };
