import "./App.css";
import logo from "./assets/git_logo.png";
import { LogoImage } from "./components/logo/Logo.styled";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <LogoImage src={logo} alt="logo" className="App-logo" />
      </header>
    </div>
  );
};

export default App;
