import React from "react";
import logo from "./assets/git_logo.png";
import { Counter } from "./features/counter/Counter";
import "./App.css";
import { LogoImage } from "./components/logo/Logo.styled";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <LogoImage src={logo} alt="logo" />
        <Counter />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
      </header>
    </div>
  );
}

export default App;

// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDlOv95vHVrPKPwXFTFe05LxTM6DnMB4M8",
  authDomain: "bk-github-searcher.firebaseapp.com",
  projectId: "bk-github-searcher",
  storageBucket: "bk-github-searcher.appspot.com",
  messagingSenderId: "838449441464",
  appId: "1:838449441464:web:d6ddc4e77220b05c9ee24d",
  measurementId: "G-6VV3MS63ST",
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
