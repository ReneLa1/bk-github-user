import { useMemo } from "react";
import { auth } from "./auth";

export const useAuth = () => {
  const user = auth.currentUser;

  return useMemo(() => ({ user }), [user]);
};
