import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDlOv95vHVrPKPwXFTFe05LxTM6DnMB4M8",
  authDomain: "bk-github-searcher.firebaseapp.com",
  projectId: "bk-github-searcher",
  storageBucket: "bk-github-searcher.appspot.com",
  messagingSenderId: "838449441464",
  appId: "1:838449441464:web:d6ddc4e77220b05c9ee24d",
  measurementId: "G-6VV3MS63ST",
};

//initialize firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
