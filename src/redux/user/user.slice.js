import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "user",
  initialState: { user: null, token: null },
  reducers: {
    saveUser: (state, { payload }) => {
      state.user = payload;
      state.token = payload.token;
    },
  },
});

export const { saveUser } = slice.actions;

export default slice.reducer;

export const selectCurrentUser = (state) => state.auth.user;
